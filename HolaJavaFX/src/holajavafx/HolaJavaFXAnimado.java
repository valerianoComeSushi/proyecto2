/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package holajavafx;


import javafx.animation.Interpolator;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author usuario
 */
public class HolaJavaFXAnimado extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        
        String mensaje="                           hola____sdjd ";
        Text nodoTexto=new Text(mensaje);
        nodoTexto.setTextOrigin(VPos.TOP);
        nodoTexto.setTextAlignment(TextAlignment.JUSTIFY);
        nodoTexto.setWrappingWidth(1000);
        nodoTexto.setFill(Color.rgb(255,0,255));
        nodoTexto.setFont(Font.font("Arial",FontWeight.BOLD,70));
        
        
        Pane panel=new Pane(nodoTexto);
        panel.setPrefWidth(600);
        panel.setPrefHeight(75);
        panel.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        
        
        
        TranslateTransition transicionTranslacion=new TranslateTransition(new Duration(10000),nodoTexto);
        transicionTranslacion.setToX(-nodoTexto.getWrappingWidth());
        transicionTranslacion.setInterpolator(Interpolator.LINEAR);
        transicionTranslacion.setCycleCount(Timeline.INDEFINITE);
        
       
        
        Scene scene = new Scene(panel);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
       transicionTranslacion.play();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
