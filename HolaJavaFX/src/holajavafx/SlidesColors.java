/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package holajavafx;

import javafx.application.Application;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author usuario
 */
public class SlidesColors extends Application {

    private Rectangle rectanguloMuestra;
    private IntegerProperty valorR;
    private IntegerProperty valorG;
    private IntegerProperty valorB;

    private Text colorR;
    private Text colorG;
    private Text colorB;

    private Slider sliderR;
    private Slider sliderG;
    private Slider sliderB;

    private Group grupo;
    private Scene escena;
    
    private Rectangle rectangulo;
     private Rectangle rectanguloInicio;
     private Color colorMuestra;
     
     private Line separador1;
     LinearGradient gradiente;

    public SlidesColors() {

        //titulo=new Text(45, 12, "Selector de colores RGB");
        // para poder hacer un bind de una cariable con otra, estas deben ser te tipo Propertiy();
        valorR = new SimpleIntegerProperty(255);
        valorG = new SimpleIntegerProperty(255);
        valorB = new SimpleIntegerProperty(255);

        colorR = new Text("fff");
        colorR.setLayoutX(18);
        colorR.setLayoutY(69);
        colorR.setTextOrigin(VPos.TOP);//que parte del texto se usa de referencia
        colorR.setFill(Color.RED);//color del texto

        colorG = new Text("colorG: ");
        colorG.setLayoutX(18);
        colorG.setLayoutY(125);
        colorG.setTextOrigin(VPos.TOP);//que parte del texto se usa de referencia
        colorG.setFill(Color.GREEN);//color del texto

        colorB = new Text("colorR: ");
        colorB.setLayoutX(18);
        colorB.setLayoutY(190);
        colorB.setTextOrigin(VPos.TOP);//que parte del texto se usa de referencia
        colorB.setFill(Color.BLUE);//color del texto

        sliderR = new Slider();
        sliderR.setMin(0);
        sliderR.setMax(255);
        sliderR.setValue(255);
        // colorR.setFont(Font.font("Arial"),Font.);
        sliderR.setLayoutX(135);
        sliderR.setLayoutY(69);
        sliderR.setPrefWidth(162);

        sliderG = new Slider();
        sliderG.setMin(0);
        sliderG.setMax(255);
        sliderG.setValue(255);
        sliderG.setLayoutX(135);
        sliderG.setLayoutY(125);
        sliderG.setPrefWidth(162);

        sliderB = new Slider();
        sliderB.setMin(0);
        sliderB.setMax(255);
        sliderB.setValue(255);
        sliderB.setLayoutX(135);
        sliderB.setLayoutY(190);
        sliderB.setPrefWidth(162);
        
      
          
          sliderR.valueProperty().addListener(new InvalidationListener() {
           
            @Override
            public void invalidated(Observable observable) {
               
                colorMuestra=Color.rgb(valorR.intValue(), valorG.intValue(),valorB.intValue(),0.5);
                rectangulo.setFill(colorMuestra);
            }
        });
          
          
            sliderG.valueProperty().addListener(new InvalidationListener() {
           
            @Override
            public void invalidated(Observable observable) {
               
                colorMuestra=Color.rgb(valorR.intValue(), valorG.intValue(),valorB.intValue());
                rectangulo.setFill(colorMuestra);
            }
        });
            
              sliderB.valueProperty().addListener(new InvalidationListener() {
           
            @Override
            public void invalidated(Observable observable) {
               
                colorMuestra=Color.rgb(valorR.intValue(), valorG.intValue(),valorB.intValue());
                rectangulo.setFill(colorMuestra);
            }
        });
          
               rectangulo=new Rectangle(20,219,276,90);
          rectangulo.setArcHeight(20);
          rectangulo.setArcWidth(20);
          rectangulo.setFill(Color.TRANSPARENT);
          rectangulo.setStroke(colorMuestra);
          
//          rectanguloInicio=new Rectangle(0,219,0,90);
//          rectanguloInicio.setArcHeight(20);
//          
          
          separador1 =new Line(20,219,276,90);
          
          Stop stops []={new Stop(0,Color.GREEN),new Stop(0, Color.PINK)};
          gradiente=new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, stops);
          

        grupo = new Group(colorR, colorG, colorB, sliderR, sliderG,sliderB,rectangulo,separador1);
        escena = new Scene(grupo, 320, 343);
        //escena.setFill(Color.TRANSPARENT);

        /*
       
       
       hay dos tipos de bind: 
       
       a.bind(b) si cambia b cambia a pero no al contrario
       a.bindBidirectional(b) si cambia b cambia a y vicebersa
       
         */
        sliderR.valueProperty().bindBidirectional(valorR);
        // colorR.textProperty().bind(new SimpleStringProperty(("Rojo: ").concat(valorR.asString().toString())));
        colorR.textProperty().bind(new SimpleStringProperty("Rojo: ").concat(valorR.asString()));
        
         sliderG.valueProperty().bindBidirectional(valorG);
         colorG.textProperty().bind(new SimpleStringProperty("Verde: ").concat(valorG.asString()));
         
          sliderB.valueProperty().bindBidirectional(valorB);
          colorB.textProperty().bind(new SimpleStringProperty("Azul: ").concat(valorB.asString()));
          
       

    }

    @Override
    public void start(Stage primaryStage) {
        /*
        Existe un nodo que contiene al resto, el root, que puede ser de cualquier tipo.
        Por norma general,, excepto el root, todos tienen un nodo padre e hijos.
         */

       // primaryStage.initStyle(StageStyle.TRANSPARENT);
        StackPane root = new StackPane();// nodo raíz del que cuelgan todos, este no tiene padre, es decir no cuelga de ninguno..

        primaryStage.setTitle("Slide Color");
        primaryStage.setScene(escena);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //new SlidesColors();
        launch(args);
    }

}
